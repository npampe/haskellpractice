doubleHead x y = head x + head y

longerList x y = 
	if length x > length y
	then x
	else y
	
isItThere x y = 
	if elem x y
	then "yes"
	else "no"
	
evenOdd n = [2,4..n*2] ++ [1,3..n*2]